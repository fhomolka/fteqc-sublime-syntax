# FTEQC Sublime Syntax

## How to Use

1. Open up Sublime
2. Preferences->Browse Packages
3. Open up the `User` folder
4. Put all files into an `fteqc` subdirectory
